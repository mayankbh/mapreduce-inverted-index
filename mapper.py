#!/bin/python2

import sys
import os
import re
regex=re.compile(r'[a-zA-z0-9]+\(.*')

for line in sys.stdin:
    filename = os.environ["map_input_file"]
    if '/' in filename:
        filename = filename.split('/')[-1]
    line = line.strip()
    for word in line.split():
        if regex.match(word):
	    word = word.split('(')[0]
            print '%s\t%s' % (word, filename)
