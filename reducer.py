#!/bin/python2

import sys

current_word = None
current_files = []

for line in sys.stdin:
    #Receives word, file pairs
    split_line = line.split('\t')
    word = split_line[0]
    filen = split_line[1]
    filen = filen.replace('\n', '')
    if current_word == None or word != current_word:
        if current_word != None:
	    print current_word,
	    for filename in current_files:
	        print '\t%s' % filename,
	    print ''
	current_word = word
	current_files = []
    if filen not in current_files:
        current_files.append(filen)

if current_word != None:
    print current_word,
    for filename in current_files:
        print '\t%s' % filename,
    print ''

